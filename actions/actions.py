# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"
import json
from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher


class ActionCheckHours(Action):

    def __init__(self):
        self.opening_hours = self.load_opening_hours()

    def name(self) -> Text:
        return "check_hours"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        day = next(tracker.get_latest_entity_values('day')).lower()
        time = next(tracker.get_latest_entity_values('time'))

        if day.lower() not in self.opening_hours:
            dispatcher.utter_message(text=f"Sorry, I didn't understand what day you meant.")
            return []

        open_hour = self.opening_hours[day]['open']
        close_hour = self.opening_hours[day]['close']
        if open_hour == close_hour:
            dispatcher.utter_message(text="Sorry, we are not working on this day.")
            return []

        try:
            hour = int(time)
        except:
            dispatcher.utter_message(text=f"Sorry, I didn't understand what hour you meant.")
            return []

        if open_hour <= hour and close_hour > hour:
            dispatcher.utter_message(text=f"Yes, we are open.")
        elif close_hour <= hour:
            dispatcher.utter_message(text=f"Sorry, we close at {close_hour}.")
        else:
            dispatcher.utter_message(text=f"Sorry, we open at {open_hour}.")

        return []

    @staticmethod
    def load_opening_hours():
        with open('./opening_hours.json') as opening_hours_file:
            opening_hours_json = json.load(opening_hours_file)
            return {key.lower(): value for key, value in opening_hours_json.items()}


class ActionDescribeMenu(Action):

    def __init__(self):
        self.menu_description = self.load_menu_description()

    def name(self) -> Text:
        return "describe_menu"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        dispatcher.utter_message(text=self.menu_description)

        return []

    @staticmethod
    def load_menu_description():
        with open('./menu.json') as menu_file:
            menu_json = json.load(menu_file)
            return ("Menu:\n" +
                    '\n'.join([f'{item["name"]} price: {item["price"]} preparation time: {item["preparation_time"]}h'
                               for item in menu_json]))


