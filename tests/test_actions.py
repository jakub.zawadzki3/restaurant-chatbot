from unittest import TestCase
from unittest.mock import patch, mock_open, MagicMock, call
from actions.actions import ActionCheckHours, ActionDescribeMenu


class TestActionCheckHours(TestCase):

    @patch('builtins.open', mock_open(read_data='{"Monday": {"open": 0, "close": 0}}'))
    def test_day_when_closed(self):
        self.check_run([('Monday',).__iter__(), ('5',).__iter__()], "Sorry, we are not working on this day.")

    @patch('builtins.open', mock_open(read_data='{"Monday": {"open": 8, "close": 14}}'))
    def test_open_hours(self):
        self.check_run([('Monday',).__iter__(), ('8',).__iter__()], "Yes, we are open.")

    @patch('builtins.open', mock_open(read_data='{"Monday": {"open": 8, "close": 14}}'))
    def test_after_closing(self):
        self.check_run([('Monday',).__iter__(), ('14',).__iter__()], "Sorry, we close at 14.")

    @patch('builtins.open', mock_open(read_data='{"Monday": {"open": 8, "close": 14}}'))
    def test_before_opening(self):
        self.check_run([('Monday',).__iter__(), ('5',).__iter__()], "Sorry, we open at 8.")

    @patch('builtins.open', mock_open(read_data='{"Monday": {"open": 8, "close": 14}}'))
    def test_invalid_day(self):
        self.check_run([('not_a_day',).__iter__(), ('5',).__iter__()],
                       "Sorry, I didn't understand what day you meant.")

    @patch('builtins.open', mock_open(read_data='{"Monday": {"open": 8, "close": 14}}'))
    def test_invalid_hour(self):
        self.check_run([('Monday',).__iter__(), ('not_an_hour',).__iter__()],
                       "Sorry, I didn't understand what hour you meant.")

    @staticmethod
    def check_run(entities_values, expected_message):
        action = ActionCheckHours()
        dispatcher_mock = MagicMock()
        tracker_mock = MagicMock()
        tracker_mock.get_latest_entity_values.side_effect = entities_values

        action.run(dispatcher_mock, tracker_mock, {})

        dispatcher_mock.utter_message.assert_called_once_with(text=expected_message)
        tracker_mock.get_latest_entity_values.assert_has_calls([call('day'), call('time')])


class TestActionDescribeMenu(TestCase):

    @patch('builtins.open', mock_open(read_data='''[
    {
      "name": "Lasagne",
      "price": 16,
      "preparation_time": 1
    },
    {
      "name": "Pizza",
      "price": 12,
      "preparation_time": 0.5
    }
    ]'''))
    def test_invalid_hour(self):
        action = ActionDescribeMenu()
        dispatcher_mock = MagicMock()

        action.run(dispatcher_mock, MagicMock(), {})

        dispatcher_mock.utter_message.assert_called_once_with(text='Menu:\n'
                                                                   'Lasagne price: 16 preparation time: 1h\n'
                                                                   'Pizza price: 12 preparation time: 0.5h')
