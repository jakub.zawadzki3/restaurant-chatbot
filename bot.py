import slack
import os
import requests
import json
from pathlib import Path
from dotenv import load_dotenv
from flask import Flask
from slackeventsapi import SlackEventAdapter

env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)
app = Flask(__name__)
slack_event_adapter = SlackEventAdapter(os.environ['SLACK_SECRET'], '/slack/events', app)

client = slack.WebClient(token=os.environ['SLACK_TOKEN'])
BOT_ID = client.api_call('auth.test')['user_id']
RASA_API = os.environ['RASA_API_URL']


@slack_event_adapter.on('message')
def message(payload):
    print(payload)
    event = payload.get('event', {})
    channel_id = event.get('channel')
    user_id = event.get('user')
    text = event.get('text')

    if BOT_ID != user_id:
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        response_text = requests.request('POST', url=RASA_API, headers=headers,
                                         data=json.dumps({"sender": channel_id, "message": text})).json()
        print(response_text)
        if response_text:
            client.chat_postMessage(channel=channel_id, text=response_text[0].get('text'))


if __name__ == '__main__':
    app.run(debug=True)
