## Restaurant Chatbot for Slack

### How to start:

- Copy .example_env to .env and set slack token and secret
- Execute following commands:
  - `pip install -r requirements.txt`
  - `python -m rasa init`
  - `rasa train`
  - `rasa run actions &`
  - `rasa run -m models --enable-api &`
  - `python bot &`
- To make your bot service accessible by Slack use e.g. `ngrok`
    
  - `./ngrok http 5000`

### How to run tests:

- `rasa test`
- `python -m unittest tests/test_actions.py`

### Example of Slack App manifest:

```
_metadata:
  major_version: 1
  minor_version: 1
display_information:
  name: restaurant-chat-bot
features:
  app_home:
    home_tab_enabled: false
    messages_tab_enabled: true
    messages_tab_read_only_enabled: false
  bot_user:
    display_name: restaurant-chat-bot
    always_online: true
oauth_config:
  scopes:
    bot:
      - chat:write
      - channels:read
      - channels:history
      - im:history
      - im:read
      - im:write
settings:
  event_subscriptions:
    request_url: http://a7b5-2a02-a31d-a038-7c00-cd92-d25-1cc4-623e.ngrok.io/slack/events
    bot_events:
      - message.channels
      - message.im
  org_deploy_enabled: false
  socket_mode_enabled: false
  token_rotation_enabled: false
````